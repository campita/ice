package com.miguelcg.ice.modelo.propietario;

public class Alergia {

    private long id;
    private String nombre;
    
    public long getId() {
	return id;
    }
    
    public String getNombre() {
	return nombre;
    }
    
    public void setId(long id) {
	this.id = id;
    }
    
    public void setNombre(String nombre) {
	this.nombre = nombre;
    }
    
}
