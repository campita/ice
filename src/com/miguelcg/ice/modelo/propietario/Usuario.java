package com.miguelcg.ice.modelo.propietario;

public class Usuario {

    private String nombre;

    private String fechaNacimiento;

    private float altura;
    private float peso;

    private boolean donante;
    private GrupoSanguineo grupoSanguineo;

    public GrupoSanguineo getGrupoSanguineo() {
	return grupoSanguineo;
    }

    public String getNombre() {
	return nombre;
    }

    public float getPeso() {
	return peso;
    }

    public boolean isDonante() {
	return donante;
    }

    public void setDonante(boolean donante) {
	this.donante = donante;
    }

    public void setGrupoSanguineo(GrupoSanguineo grupoSanguineo) {
	this.grupoSanguineo = grupoSanguineo;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public void setPeso(float peso) {
	this.peso = peso;
    }

    public String getFechaNacimiento() {
	return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
	this.fechaNacimiento = fechaNacimiento;
    }

    public float getAltura() {
	return altura;
    }

    public void setAltura(float altura) {
	this.altura = altura;
    }

}
