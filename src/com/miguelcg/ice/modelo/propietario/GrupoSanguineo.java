package com.miguelcg.ice.modelo.propietario;

public enum GrupoSanguineo {

    A_MAS, A_MENOS, B_MAS, B_MENOS, AB_MAS, AB_MENOS, CERO_MAS, CERO_MENOS

}
