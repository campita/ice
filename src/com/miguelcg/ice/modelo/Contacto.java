package com.miguelcg.ice.modelo;

import android.net.Uri;

public class Contacto {
    
    private long id;
    private String nombre;
    private String telefono;
    private Uri uri;
    
    public void setId(long id) {
	this.id = id;
    }
    
    public void setNombre(String nombre) {
	this.nombre = nombre;
    }
    
    public void setTelefono(String telefono) {
	this.telefono = telefono;
    }
    
    public long getId() {
	return id;
    }
    
    public String getNombre() {
	return nombre;
    }
    
    public String getTelefono() {
	return telefono;
    }
    
    public Uri getUri() {
	return uri;
    }
    
    public void setUri(Uri uri) {
	this.uri = uri;
    }
}
