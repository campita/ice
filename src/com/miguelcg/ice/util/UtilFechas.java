package com.miguelcg.ice.util;

import java.util.Calendar;

import android.widget.DatePicker;

public class UtilFechas {

    public static String getSelectedDateFormattedFromDatePicker(
	    DatePicker dpFecha) {
	final String SEPARATOR = "/";
	String fecha = "";

	int dia = dpFecha.getDayOfMonth();
	int mes = (dpFecha.getMonth() + 1);
	int ano = dpFecha.getYear();

	if (dia < 10) {
	    fecha += "0" + dia;
	} else {
	    fecha += dia;
	}

	fecha += SEPARATOR;

	if (mes < 10) {
	    fecha += "0" + mes;
	} else {
	    fecha += mes;
	}

	fecha += SEPARATOR;

	fecha += ano;

	return fecha;
    }

    public static int getEdadFromFechaString(String fechaNacimiento) {
	String[] fechaPartes = fechaNacimiento.split("/");

	int diaNacimiento = Integer.parseInt(fechaPartes[0]);
	int mesNacimiento = Integer.parseInt(fechaPartes[1]);
	int anoNacimiento = Integer.parseInt(fechaPartes[2]);

	int diaActual = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	int mesActual = (Calendar.getInstance().get(Calendar.MONTH) + 1);
	int anoActual = Calendar.getInstance().get(Calendar.YEAR);

	int anos = anoActual - anoNacimiento;
	if (mesNacimiento > mesActual || ((mesNacimiento == mesActual) && (diaNacimiento > diaActual))) {
	    anos--;
	}

	return anos;
    }

}
