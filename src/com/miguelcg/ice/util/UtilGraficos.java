package com.miguelcg.ice.util;

import java.util.Calendar;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class UtilGraficos {

    public static void deTextViewAEditText(TextView tv, EditText et) {
	et.setText(tv.getText());

	tv.setVisibility(View.GONE);
	et.setVisibility(View.VISIBLE);
    }

    public static void deEditTextATextView(EditText et, TextView tv) {
	tv.setText(et.getText());

	et.setVisibility(View.GONE);
	tv.setVisibility(View.VISIBLE);
    }

    public static void deTextViewASpinner(TextView tv, Spinner sp) {
	SpinnerAdapter spAdapter = sp.getAdapter();

	String eleccionActual = tv.getText().toString();

	for (int i = 0; i < spAdapter.getCount(); i++) {
	    if (spAdapter.getItem(i).equals(eleccionActual)) {
		sp.setSelection(i);
		break;
	    }
	}

	tv.setVisibility(View.GONE);
	sp.setVisibility(View.VISIBLE);
    }

    public static void deSpinnerATextView(Spinner sp, TextView tv) {
	tv.setText(sp.getSelectedItem().toString());

	sp.setVisibility(View.GONE);
	tv.setVisibility(View.VISIBLE);
    }

    public static void cambiarMenu(Activity actividad, Menu menuActual,
	    int idNuevoMenu) {
	menuActual.clear();
	MenuInflater inflater = actividad.getMenuInflater();
	inflater.inflate(idNuevoMenu, menuActual);
    }

    public static void deTextViewADatePicker(TextView tv, DatePicker dp) {
	String[] fecha = tv.getText().toString().split("/");

	int dia;
	int mes;
	int ano;

	if (fecha.length == 3) {
	    dia = Integer.parseInt(fecha[0]);
	    mes = (Integer.parseInt(fecha[1]) - 1);
	    ano = Integer.parseInt(fecha[2]);
	} else {
	    Calendar c = Calendar.getInstance();

	    dia = c.get(Calendar.DAY_OF_MONTH);
	    mes = c.get(Calendar.MONTH);
	    ano = c.get(Calendar.YEAR);
	}

	dp.setCalendarViewShown(false);
	dp.init(ano, mes, dia, null);

	tv.setVisibility(View.GONE);
	dp.setVisibility(View.VISIBLE);
    }

    public static void deDatePickerATextView(DatePicker dp, TextView tv) {

	tv.setText(UtilFechas.getSelectedDateFormattedFromDatePicker(dp));

	dp.setVisibility(View.GONE);
	tv.setVisibility(View.VISIBLE);
    }

}
