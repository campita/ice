package com.miguelcg.ice.controladores;

import com.miguelcg.ice.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PrincipalActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.principal);
    }

    public void clickInformacionPersonal(View v) {
	Intent i = new Intent(this, InformacionActivity.class);
	startActivity(i);
    }

    public void clickContactos(View v) {
	Intent i = new Intent(this, ContactosActivity.class);
	startActivity(i);
    }
}
