package com.miguelcg.ice.controladores.dialogos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.miguelcg.ice.R;

public class AnadirEnfermedadDialogo extends DialogFragment {
    
    private EditText etEnfermedad;
    
    private View v;
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

	LayoutInflater inflater = getActivity().getLayoutInflater();

	builder.setView(v = inflater.inflate(R.layout.dialogo_anadir_enfermedad, null)).setTitle(R.string.mensaje_introducir_enfermedad)
		
		.setPositiveButton(R.string.anadir, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {

	               etEnfermedad = (EditText) v.findViewById(R.id.etAnadirEnfermedad);
	               
	               Intent i = new Intent();
        	       Bundle bundle = new Bundle();
        	       bundle.putString("etEnfermedad", etEnfermedad.getText().toString());
        	       i.putExtras(bundle);
        	       
	               getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
	           }
	       })
	       .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	               AnadirEnfermedadDialogo.this.getDialog().cancel();
	           }
	       });
	
        return builder.create();
    }
}
