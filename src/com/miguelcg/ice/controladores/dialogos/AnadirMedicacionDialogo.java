package com.miguelcg.ice.controladores.dialogos;

import com.miguelcg.ice.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class AnadirMedicacionDialogo extends DialogFragment {
    
    private EditText etMedicacion;
    
    private View v;
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

	LayoutInflater inflater = getActivity().getLayoutInflater();

	builder.setView(v = inflater.inflate(R.layout.dialogo_anadir_medicacion, null)).setTitle(R.string.mensaje_introducir_medicacion)
		
		.setPositiveButton(R.string.anadir, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {

	               etMedicacion = (EditText) v.findViewById(R.id.etAnadirMedicacion);
	               
	               Intent i = new Intent();
        	       Bundle bundle = new Bundle();
        	       bundle.putString("etMedicacion", etMedicacion.getText().toString());
        	       i.putExtras(bundle);
        	       
	               getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
	           }
	       })
	       .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	               AnadirMedicacionDialogo.this.getDialog().cancel();
	           }
	       });
	
        return builder.create();
    }

}
