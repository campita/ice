package com.miguelcg.ice.controladores.pestanas;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.miguelcg.ice.R;
import com.miguelcg.ice.datos.ICEManager;
import com.miguelcg.ice.modelo.propietario.GrupoSanguineo;
import com.miguelcg.ice.modelo.propietario.Usuario;
import com.miguelcg.ice.util.UtilFechas;
import com.miguelcg.ice.util.UtilGraficos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class PestanaGeneral extends Fragment {
    
    private Menu menu;
    
    private TextView tvNombre;
    private EditText etNombre;
    
    private TextView tvFecha;
    private DatePicker dpFecha;
    
    private TextView tvAltura;
    private EditText etAltura;
    
    private TextView tvPeso;
    private EditText etPeso;
    
    private TextView tvDonante;
    private Spinner spDonante;
    
    private TextView tvGrupoSanguineo;
    private Spinner spGrupoSanguineo;
    
    private TextView tvEdad;
    private TextView tvLabelEdad;
    
    private boolean editando = false;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
	
	setHasOptionsMenu(true);
	
	return inflater.inflate(R.layout.pestana_general, container, false);
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        cargarViews();
        cargarDatosUsuario();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	super.onCreateOptionsMenu(menu, inflater);
	if(!editando){
	    inflater.inflate(R.menu.menu_editar, menu);
	}else{
	    inflater.inflate(R.menu.menu_aceptar, menu);
	}
        this.menu = menu;
    }
    
    /**
     * Se cargan los views y se inicializan con adapters, fechas maximas, etc.
     */
    private void cargarViews(){
	tvNombre = (TextView) getActivity().findViewById(R.id.tvNombre);
	etNombre = (EditText) getActivity().findViewById(R.id.etNombre);
	
	tvFecha = (TextView) getActivity().findViewById(R.id.tvFechaNacimiento);
	dpFecha = (DatePicker) getActivity().findViewById(R.id.dPFechaNacimiento);
	dpFecha.setMaxDate(new GregorianCalendar(
		Calendar.getInstance().get(Calendar.YEAR), 
		Calendar.getInstance().get(Calendar.MONTH), 
		Calendar.getInstance().get(Calendar.DAY_OF_MONTH)).getTimeInMillis());
	
	tvAltura = (TextView) getActivity().findViewById(R.id.tvAltura);
	etAltura = (EditText) getActivity().findViewById(R.id.etAltura);
	
	tvPeso = (TextView) getActivity().findViewById(R.id.tvPeso);
	etPeso = (EditText) getActivity().findViewById(R.id.etPeso);
	
	tvDonante = (TextView) getActivity().findViewById(R.id.tvDonante);
	spDonante = (Spinner) getActivity().findViewById(R.id.spDonante);
	spDonante.setAdapter(ArrayAdapter.createFromResource(
		getActivity(), 
		R.array.si_no_array, 
		android.R.layout.simple_spinner_item));
	
	tvGrupoSanguineo = (TextView) getActivity().findViewById(R.id.tvGrupoSanguineo);
	spGrupoSanguineo = (Spinner) getActivity().findViewById(R.id.spGrupoSanguineo);
	spGrupoSanguineo.setAdapter(ArrayAdapter.createFromResource(
		getActivity(),
		R.array.grupos_sanguineos_array,
		android.R.layout.simple_spinner_item));
	
	tvLabelEdad = (TextView) getActivity().findViewById(R.id.tvLabelEdad);
	tvEdad = (TextView) getActivity().findViewById(R.id.tvEdad);
	
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
	case R.id.mnEditarInformacion:
	    editando = true;
	    cambiarAEditText();
	    UtilGraficos.cambiarMenu(getActivity(), menu, R.menu.menu_aceptar);
	    break;
	    
	case R.id.mnConfirmarEdicion:
	    editando = false;
	    guardarCambios();
	    cambiarATextViews();
	    UtilGraficos.cambiarMenu(getActivity(), menu, R.menu.menu_editar);
	    break;
	}
	return super.onOptionsItemSelected(item);
    }
 
    private void cambiarAEditText() {
	tvLabelEdad.setVisibility(View.GONE);
	tvEdad.setVisibility(View.GONE);
	
	UtilGraficos.deTextViewAEditText(tvNombre, etNombre);
	UtilGraficos.deTextViewADatePicker(tvFecha, dpFecha);
	UtilGraficos.deTextViewAEditText(tvAltura, etAltura);
	UtilGraficos.deTextViewAEditText(tvPeso, etPeso);

	UtilGraficos.deTextViewASpinner(tvDonante, spDonante);
	UtilGraficos.deTextViewASpinner(tvGrupoSanguineo, spGrupoSanguineo);
	
	etNombre.requestFocus();
    }

    private void cambiarATextViews() {
	tvLabelEdad.setVisibility(View.VISIBLE);
	tvEdad.setVisibility(View.VISIBLE);
	
	UtilGraficos.deEditTextATextView(etNombre, tvNombre);
	UtilGraficos.deDatePickerATextView(dpFecha, tvFecha);
	
	tvEdad.setText(String.valueOf(
		UtilFechas.getEdadFromFechaString(
			UtilFechas.getSelectedDateFormattedFromDatePicker(dpFecha))));
	
	UtilGraficos.deEditTextATextView(etAltura, tvAltura);
	UtilGraficos.deEditTextATextView(etPeso, tvPeso);
 	
	UtilGraficos.deSpinnerATextView(spDonante, tvDonante);
	UtilGraficos.deSpinnerATextView(spGrupoSanguineo, tvGrupoSanguineo);
     }
    
    private void cargarDatosUsuario() {
	Usuario usuario = new ICEManager(getActivity()).getInformacionUsuario();
	
	tvNombre.setText(usuario.getNombre());
	tvFecha.setText(usuario.getFechaNacimiento());
	tvEdad.setText(String.valueOf(UtilFechas.getEdadFromFechaString(usuario.getFechaNacimiento())));
	tvAltura.setText(String.valueOf(usuario.getAltura()));
	tvPeso.setText(String.valueOf(usuario.getPeso()));
	tvDonante.setText(usuario.isDonante() ? 
		getResources().getStringArray(R.array.si_no_array)[0] : 
		    getResources().getStringArray(R.array.si_no_array)[1]);
	
	tvGrupoSanguineo.setText(getResources().
		getStringArray(R.array.grupos_sanguineos_array)[usuario.getGrupoSanguineo().ordinal()]);
    }
    
    private void guardarCambios() {
	Usuario usuario = new Usuario();
	
	usuario.setNombre(etNombre.getText().toString());
	usuario.setFechaNacimiento(UtilFechas.getSelectedDateFormattedFromDatePicker(dpFecha));
	usuario.setAltura(Float.parseFloat(etAltura.getText().toString()));
	usuario.setPeso(Float.parseFloat(etPeso.getText().toString()));
	usuario.setDonante(
		spDonante.getSelectedItem().toString().equals(
			getResources().getStringArray(R.array.si_no_array)[0]) ? true : false);
	usuario.setGrupoSanguineo(GrupoSanguineo.values()[spGrupoSanguineo.getSelectedItemPosition()]);
	
	new ICEManager(getActivity()).actualizarInformacionUsuario(usuario);
    }
    
    
}

