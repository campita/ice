package com.miguelcg.ice.controladores.pestanas;

import com.miguelcg.ice.R;
import com.miguelcg.ice.datos.ICEManager;
import com.miguelcg.ice.util.UtilGraficos;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class PestanaOtra extends Fragment {

    private TextView tvOtra;
    private EditText etOtra;

    private boolean editando = false;
    
    private Menu menu;
    
    private InputMethodManager imm;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {

	imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	
	setHasOptionsMenu(true);
	
	return inflater.inflate(R.layout.pestana_otra, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	super.onActivityCreated(savedInstanceState);

	cargarViews();
	cargarDatos();
    }

    private void cargarViews() {
	
	tvOtra = (TextView) getActivity().findViewById(R.id.tvOtraInformacion);
	etOtra = (EditText) getActivity().findViewById(R.id.etOtraInformacion);
    }
    
    private void cargarDatos(){
	String otraInformacion = new ICEManager(getActivity()).getOtraInformacion();
	
	if(otraInformacion.isEmpty()){
	    tvOtra.setText(getResources().getString(R.string.otra_informacion_vacio));
	}else{
	    tvOtra.setText(otraInformacion);
	}
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	super.onCreateOptionsMenu(menu, inflater);
	if(!editando){
	    inflater.inflate(R.menu.menu_editar, menu);
	}else{
	    inflater.inflate(R.menu.menu_aceptar, menu);
	}
        this.menu = menu;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
	case R.id.mnEditarInformacion:
	    editando = true;
	    cambiarAEditText();
	    UtilGraficos.cambiarMenu(getActivity(), menu, R.menu.menu_aceptar);
	    break;
	    
	case R.id.mnConfirmarEdicion:
	    editando = false;

	    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	    guardarCambios();
	    cambiarATextViews();
	    UtilGraficos.cambiarMenu(getActivity(), menu, R.menu.menu_editar);
	    break;
	}
	return super.onOptionsItemSelected(item);
    }

    private void cambiarAEditText() {
	if(tvOtra.getText().toString().equals(getResources().getString(R.string.otra_informacion_vacio))){
	    tvOtra.setText("");
	}
	UtilGraficos.deTextViewAEditText(tvOtra, etOtra);
	
	etOtra.requestFocus();
	
	imm.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
    }
    
    private void cambiarATextViews() {
	if(etOtra.getText().toString().trim().equals("")){
	    etOtra.setText(getResources().getString(R.string.otra_informacion_vacio));
	}
	UtilGraficos.deEditTextATextView(etOtra, tvOtra);
    }
    
    private void guardarCambios() {
	new ICEManager(getActivity()).actualizarOtraInformacion(etOtra.getText().toString().trim());
    }

}
