package com.miguelcg.ice.controladores.pestanas;

import com.miguelcg.ice.R;
import com.miguelcg.ice.controladores.adapters.ListaMedicacionAdapter;
import com.miguelcg.ice.controladores.dialogos.AnadirEnfermedadDialogo;
import com.miguelcg.ice.controladores.dialogos.AnadirMedicacionDialogo;
import com.miguelcg.ice.datos.ICEManager;
import com.miguelcg.ice.modelo.propietario.Medicacion;
import com.miguelcg.ice.util.UtilGraficos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class PestanaMedicacion extends ListFragment implements OnItemClickListener{
    
public static final int DIALOG_FRAGMENT = 1;
    
    private Menu menu;
    
    private boolean editando = false;
    
    private ListView listaMedicacion;
    private ListaMedicacionAdapter adaptadorListaMedicacion;
    
    private Button btAnadir;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
	
	setHasOptionsMenu(true);
	
	return inflater.inflate(R.layout.pestana_medicacion, container, false);
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listaMedicacion = (ListView) getActivity().findViewById(android.R.id.list);
        listaMedicacion.setOnItemClickListener(this);
        
        adaptadorListaMedicacion = new ListaMedicacionAdapter(
        	getActivity(), 
        	new ICEManager(getActivity()).getMedicaciones());
        
        setListAdapter(adaptadorListaMedicacion);
        
        btAnadir = (Button) getActivity().findViewById(R.id.btAnadirMedicacion);
        btAnadir.setOnClickListener(new OnClickListener() {
	    
	    @Override
	    public void onClick(View v) {
		AnadirMedicacionDialogo amDialogo = new AnadirMedicacionDialogo();
		amDialogo.setTargetFragment(PestanaMedicacion.this, DIALOG_FRAGMENT);
		amDialogo.show(getFragmentManager(), AnadirEnfermedadDialogo.class.getName());
	    }
	});
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	super.onCreateOptionsMenu(menu, inflater);
	if(!editando){
	    inflater.inflate(R.menu.menu_editar, menu);
	}else{
	    inflater.inflate(R.menu.menu_aceptar, menu);
	}
        this.menu = menu;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
	case R.id.mnEditarInformacion:
	    editando = true;
	    btAnadir.setVisibility(View.VISIBLE);
	    adaptadorListaMedicacion.mostrarBotones();
	    UtilGraficos.cambiarMenu(getActivity(), menu, R.menu.menu_aceptar);
	    break;
	    
	case R.id.mnConfirmarEdicion:
	    editando = false;
	    btAnadir.setVisibility(View.GONE);
	    adaptadorListaMedicacion.ocultarBotones();
	    UtilGraficos.cambiarMenu(getActivity(), menu, R.menu.menu_editar);
	    break;
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	// TODO Auto-generated method stub
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	switch (requestCode) {
	case DIALOG_FRAGMENT:
	    Medicacion medicacion = new Medicacion();
	    medicacion.setNombre(data.getStringExtra("etMedicacion"));
		
		new ICEManager(getActivity()).anadirMedicacion(medicacion);
		
		adaptadorListaMedicacion.add(medicacion);
	    break;

	default:
	    break;
	}
    }

}
