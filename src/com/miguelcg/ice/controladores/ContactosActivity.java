package com.miguelcg.ice.controladores;

import com.miguelcg.ice.R;
import com.miguelcg.ice.controladores.adapters.ListaContactosAdapter;
import com.miguelcg.ice.datos.ICEManager;
import com.miguelcg.ice.modelo.Contacto;
import com.miguelcg.ice.util.UtilGraficos;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class ContactosActivity extends ListActivity implements OnItemClickListener, OnClickListener {
    
    private static final int CONTACT_PICKER_RESULT = 1001;
    
    private ListView listaContactos;
    private ListaContactosAdapter adaptadorListaContactos;
    
    private Menu menu;
    private boolean editando = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.contactos);
	
	listaContactos = (ListView) findViewById(android.R.id.list);
	listaContactos.setOnItemClickListener(this);
	
	adaptadorListaContactos = new ListaContactosAdapter(
		this, 
		new ICEManager(this).getContactos());
	
	setListAdapter(adaptadorListaContactos);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	boolean ret = super.onCreateOptionsMenu(menu);
	
	if(!editando){
	    getMenuInflater().inflate(R.menu.menu_editar, menu);
	}else{
	    getMenuInflater().inflate(R.menu.menu_aceptar, menu);
	}
        this.menu = menu;
        
        return ret;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
	case R.id.mnEditarInformacion:
	    editando = true;
	    mostrarBotones();
	    UtilGraficos.cambiarMenu(this, menu, R.menu.menu_aceptar);
	    break;
	    
	case R.id.mnConfirmarEdicion:
	    editando = false;
	    ocultarBotones();
	    UtilGraficos.cambiarMenu(this, menu, R.menu.menu_editar);
	    break;
	}
	return super.onOptionsItemSelected(item);
    }
    
    private void mostrarBotones(){
	Button btAnadirContacto = (Button) findViewById(R.id.btAnadirContacto);
	btAnadirContacto.setOnClickListener(this);
	btAnadirContacto.setVisibility(View.VISIBLE);
	
	adaptadorListaContactos.mostrarBotones();
    }
    
    private void ocultarBotones(){
	Button btAnadirContacto = (Button) findViewById(R.id.btAnadirContacto);
	btAnadirContacto.setVisibility(View.GONE);
	
	adaptadorListaContactos.ocultarBotones();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 if (resultCode == RESULT_OK) {  
	        switch (requestCode) {  
	        case CONTACT_PICKER_RESULT:  
	            
	            //Ver si se puede meter esto con el DAO
	            String id = data.getData().getLastPathSegment();
	            
	            Cursor cursor = getContentResolver().query(
        	    Contacts.CONTENT_URI,
        	    new String[] {Contacts.DISPLAY_NAME},
        	    Contacts._ID + " = ?",
        	    new String[] {id},
        	    null);
	            
	            if(cursor.moveToFirst()){
	        	String nombre = cursor.getString(cursor.getColumnIndex(Phone.DISPLAY_NAME));
	        	Contacto contacto = new Contacto();
	        	contacto.setUri(data.getData());
	        	contacto.setNombre(nombre);
	        	
	        	contacto = new ICEManager(this).insertarContacto(contacto);
	        	adaptadorListaContactos.add(contacto);
	        	//adaptadorListaContactos.notifyDataSetChanged();
	            }
	            cursor.close();
	            break;
	    }
	}
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View v, int pos, long arg3) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, adaptadorListaContactos.getItem(pos).getUri().getLastPathSegment());
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
	Intent selectorContactos = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);
	startActivityForResult(selectorContactos, CONTACT_PICKER_RESULT);
    }
}
