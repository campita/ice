package com.miguelcg.ice.controladores.adapters;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.miguelcg.ice.R;
import com.miguelcg.ice.datos.ICEManager;
import com.miguelcg.ice.modelo.propietario.Medicacion;

public class ListaMedicacionAdapter extends BaseAdapter implements OnClickListener{

    private List<Medicacion> medicaciones;
    private Context contexto;
    
    private List<ImageButton> botonesEliminar;
    
    public ListaMedicacionAdapter(Context contexto, List<Medicacion> medicaciones) {
	this.contexto = contexto;
	this.medicaciones = medicaciones;
	
	botonesEliminar = new LinkedList<ImageButton>();
    }

    @Override
    public int getCount() {
	return medicaciones.size();
    }

    @Override
    public Medicacion getItem(int pos) {
	return medicaciones.get(pos);
    }

    @Override
    public long getItemId(int id) {
	return id;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
	View v = convertView;
	if(v == null){
	    LayoutInflater vi = (LayoutInflater)contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.fila_lista_medicacion, null);
	}
	
	TextView tvNombre = (TextView) v.findViewById(R.id.tvNombreMedicacion);
	tvNombre.setText(getItem(pos).getNombre());
	
	ImageButton btEliminarMedicacion = (ImageButton) v.findViewById(R.id.btEliminarMedicacion);
	btEliminarMedicacion.setTag(getItem(pos));
	btEliminarMedicacion.setOnClickListener(this);
	botonesEliminar.add(btEliminarMedicacion);
	
	return v;
    }

    public void add(Medicacion medicacion) {
	medicaciones.add(medicacion);
	this.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
	Medicacion medicacion = (Medicacion) v.getTag();
	new ICEManager(contexto).eliminarMedicacion(medicacion);
	
	medicaciones.remove(medicacion);
	this.notifyDataSetChanged();
    }

    public void mostrarBotones() {
	for(ImageButton b : botonesEliminar){
	    b.setVisibility(View.VISIBLE);
	}
    }
    
    public void ocultarBotones(){
	for(ImageButton b : botonesEliminar){
	    b.setVisibility(View.GONE);
	}
    }
}
