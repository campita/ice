package com.miguelcg.ice.controladores.adapters;

import java.util.LinkedList;
import java.util.List;

import com.miguelcg.ice.R;
import com.miguelcg.ice.datos.ICEManager;
import com.miguelcg.ice.modelo.propietario.Alergia;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class ListaAlergiasAdapter extends BaseAdapter implements OnClickListener {
    
    private List<Alergia> alergias;
    private Context contexto;
    
    private List<ImageButton> botonesEliminar;
    
    public ListaAlergiasAdapter(Context contexto, List<Alergia> alergias) {
	this.contexto = contexto;
	this.alergias = alergias;
	
	botonesEliminar = new LinkedList<ImageButton>();
    }

    @Override
    public int getCount() {
	return alergias.size();
    }

    @Override
    public Alergia getItem(int pos) {
	return alergias.get(pos);
    }

    @Override
    public long getItemId(int id) {
	return id;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
	View v = convertView;
	if(v == null){
	    LayoutInflater vi = (LayoutInflater)contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.fila_lista_alergias, null);
	}
	
	TextView tvNombre = (TextView) v.findViewById(R.id.tvNombreAlergia);
	tvNombre.setText(getItem(pos).getNombre());
	
	ImageButton btEliminarAlergia = (ImageButton) v.findViewById(R.id.btEliminarAlergia);
	btEliminarAlergia.setTag(getItem(pos));
	btEliminarAlergia.setOnClickListener(this);
	botonesEliminar.add(btEliminarAlergia);
	
	return v;
    }

    public void add(Alergia alergia) {
	alergias.add(alergia);
	this.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
	Alergia alergia = (Alergia) v.getTag();
	new ICEManager(contexto).eliminarAlergia(alergia);
	
	alergias.remove(alergia);
	this.notifyDataSetChanged();
    }

    public void mostrarBotones() {
	for(ImageButton b : botonesEliminar){
	    b.setVisibility(View.VISIBLE);
	}
    }
    
    public void ocultarBotones(){
	for(ImageButton b : botonesEliminar){
	    b.setVisibility(View.GONE);
	}
    }
}
