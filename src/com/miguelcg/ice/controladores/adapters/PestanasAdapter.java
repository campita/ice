package com.miguelcg.ice.controladores.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.miguelcg.ice.R;
import com.miguelcg.ice.controladores.pestanas.PestanaAlergias;
import com.miguelcg.ice.controladores.pestanas.PestanaEnfermedades;
import com.miguelcg.ice.controladores.pestanas.PestanaGeneral;
import com.miguelcg.ice.controladores.pestanas.PestanaMedicacion;
import com.miguelcg.ice.controladores.pestanas.PestanaOtra;

public class PestanasAdapter extends FragmentPagerAdapter {

    private static final int NUM_PESTANAS = 5;

    public static final int PESTANA_GENERAL = 0;
    public static final int PESTANA_ALERGIAS = 1;
    public static final int PESTANA_ENFERMEDADES = 2;
    public static final int PESTANA_MEDICACIÓN = 3;
    public static final int PESTANA_OTRA = 4;
    
    private Context context;

    public PestanasAdapter(FragmentManager fm, Context context) {
	super(fm);
	this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
	switch (position) {
	case PESTANA_GENERAL:
	    return Fragment.instantiate(context, PestanaGeneral.class.getName());

	case PESTANA_ALERGIAS:
	    return Fragment.instantiate(context, PestanaAlergias.class.getName());

	case PESTANA_ENFERMEDADES:
	    return Fragment.instantiate(context, PestanaEnfermedades.class.getName());
	    
	case PESTANA_MEDICACIÓN:
	    return Fragment.instantiate(context, PestanaMedicacion.class.getName());
	    
	case PESTANA_OTRA:
	    return Fragment.instantiate(context, PestanaOtra.class.getName());

	}
	return null;
    }

    @Override
    public int getCount() {
	return NUM_PESTANAS;
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
	
	switch (position) {
	case PESTANA_GENERAL:
	    return context.getResources().getString(R.string.pestana_general);

	case PESTANA_ALERGIAS:
	    return context.getResources().getString(R.string.pestana_alergias);
	    
	case PESTANA_ENFERMEDADES:
	    return context.getResources().getString(R.string.pestana_enfermedades);
	    
	case PESTANA_MEDICACIÓN:
	    return context.getResources().getString(R.string.pestana_medicacion);
	    
	case PESTANA_OTRA:
	    return context.getResources().getString(R.string.pestana_otra);

	}
	return null;
    }

}
