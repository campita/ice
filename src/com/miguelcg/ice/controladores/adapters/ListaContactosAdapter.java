package com.miguelcg.ice.controladores.adapters;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.miguelcg.ice.R;
import com.miguelcg.ice.datos.ICEManager;
import com.miguelcg.ice.modelo.Contacto;

public class ListaContactosAdapter extends BaseAdapter implements OnClickListener {
    
    private List<Contacto> contactos;
    private Context contexto;
    
    private List<ImageButton> botonesEliminar;
    
    public ListaContactosAdapter(Context contexto, List<Contacto> contactos) {
	this.contexto = contexto;
	this.contactos = contactos;
	
	botonesEliminar = new LinkedList<ImageButton>();
    }

    @Override
    public int getCount() {
	return contactos.size();
    }

    @Override
    public Contacto getItem(int pos) {
	return contactos.get(pos);
    }

    @Override
    public long getItemId(int id) {
	return id;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
	View v = convertView;
	if(v == null){
	    LayoutInflater vi = (LayoutInflater)contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.fila_lista_contactos, null);
	}
	
	TextView tvNombre = (TextView) v.findViewById(R.id.tvNombreContacto);
	tvNombre.setText(getItem(pos).getNombre());
	
	ImageButton btEliminarContacto = (ImageButton) v.findViewById(R.id.btEliminarContacto);
	btEliminarContacto.setTag(getItem(pos));
	btEliminarContacto.setOnClickListener(this);
	botonesEliminar.add(btEliminarContacto);
	
	return v;
    }

    public void add(Contacto contacto) {
	contactos.add(contacto);
	this.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
	Contacto contacto = (Contacto) v.getTag();
	new ICEManager(contexto).eliminarContacto(contacto);
	
	contactos.remove(contacto);
	this.notifyDataSetChanged();
    }

    public void mostrarBotones() {
	for(ImageButton b : botonesEliminar){
	    b.setVisibility(View.VISIBLE);
	}
    }
    
    public void ocultarBotones(){
	for(ImageButton b : botonesEliminar){
	    b.setVisibility(View.GONE);
	}
    }
}
