package com.miguelcg.ice.controladores.adapters;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.miguelcg.ice.R;
import com.miguelcg.ice.datos.ICEManager;
import com.miguelcg.ice.modelo.propietario.Enfermedad;

public class ListaEnfermedadesAdapter extends BaseAdapter implements OnClickListener{
    
    private List<Enfermedad> enfermedades;
    private Context contexto;
    
    private List<ImageButton> botonesEliminar;
    
    public ListaEnfermedadesAdapter(Context contexto, List<Enfermedad> enfermedades) {
	this.contexto = contexto;
	this.enfermedades = enfermedades;
	
	botonesEliminar = new LinkedList<ImageButton>();
    }

    @Override
    public int getCount() {
	return enfermedades.size();
    }

    @Override
    public Enfermedad getItem(int pos) {
	return enfermedades.get(pos);
    }

    @Override
    public long getItemId(int id) {
	return id;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
	View v = convertView;
	if(v == null){
	    LayoutInflater vi = (LayoutInflater)contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.fila_lista_enfermedades, null);
	}
	
	TextView tvNombre = (TextView) v.findViewById(R.id.tvNombreEnfermedad);
	tvNombre.setText(getItem(pos).getNombre());
	
	ImageButton btEliminarEnfermedad = (ImageButton) v.findViewById(R.id.btEliminarEnfermedad);
	btEliminarEnfermedad.setTag(getItem(pos));
	btEliminarEnfermedad.setOnClickListener(this);
	botonesEliminar.add(btEliminarEnfermedad);
	
	return v;
    }

    public void add(Enfermedad enfermedad) {
	enfermedades.add(enfermedad);
	this.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
	Enfermedad enfermedad = (Enfermedad) v.getTag();
	new ICEManager(contexto).eliminarEnfermedad(enfermedad);
	
	enfermedades.remove(enfermedad);
	this.notifyDataSetChanged();
    }

    public void mostrarBotones() {
	for(ImageButton b : botonesEliminar){
	    b.setVisibility(View.VISIBLE);
	}
    }
    
    public void ocultarBotones(){
	for(ImageButton b : botonesEliminar){
	    b.setVisibility(View.GONE);
	}
    }
}
