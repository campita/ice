package com.miguelcg.ice.controladores;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.miguelcg.ice.R;
import com.miguelcg.ice.controladores.adapters.PestanasAdapter;

public class InformacionActivity extends FragmentActivity implements TabListener{
    
    private PestanasAdapter adaptadorPestanas;
    private ViewPager vpPestanas;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.informacion_personal);
	
	adaptadorPestanas = new PestanasAdapter(getSupportFragmentManager(), this);
	
	final ActionBar actionBar = getActionBar();
	
	actionBar.setHomeButtonEnabled(false);
	actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	
	vpPestanas = (ViewPager) findViewById(R.id.vpPestanas);
	vpPestanas.setAdapter(adaptadorPestanas);
	
	vpPestanas.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
	    @Override
	    public void onPageSelected(int position) {
		actionBar.setSelectedNavigationItem(position);
	    }
	});
	
	for(int i = 0; i < adaptadorPestanas.getCount(); i++){
	    actionBar.addTab(actionBar.newTab().setText(adaptadorPestanas.getPageTitle(i)).setTabListener(this));
	}
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
	vpPestanas.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {	
    }

}
