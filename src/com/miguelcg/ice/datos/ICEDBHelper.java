package com.miguelcg.ice.datos;

import com.miguelcg.ice.datos.ICEContract.AlergiaContract;
import com.miguelcg.ice.datos.ICEContract.ContactoContract;
import com.miguelcg.ice.datos.ICEContract.EnfermedadContract;
import com.miguelcg.ice.datos.ICEContract.MedicacionContract;
import com.miguelcg.ice.datos.ICEContract.UsuarioContract;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ICEDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ICE.db";

    private static final String TIPO_TEXTO = " TEXT";
//    private static final String TIPO_REAL = " REAL";
//    private static final String TIPO_ENTERO = " INTEGER";
    
    private static final String COMA = ",";

    //CREAR TABLAS
    private static final String SQL_CREAR_TABLA_CONTACTOS = 
	    "CREATE TABLE " + ContactoContract.NOMBRE_TABLA + " (" 
		    + ContactoContract._ID + " INTEGER PRIMARY KEY," 
		    + ContactoContract.COLUMNA_NOMBRE + TIPO_TEXTO + COMA 
		    + ContactoContract.COLUMNA_URI + TIPO_TEXTO + " )";

//    private static final String SQL_CREAR_TABLA_USUARIO = 
//	    "CREATE TABLE " + UsuarioContract.NOMBRE_TABLA + " (" 
//		    + UsuarioContract._ID + "INTEGER PRIMARY KEY,"
//		    + UsuarioContract.COLUMNA_NOMBRE + TIPO_TEXTO + COMA
////		    + UsuarioContract.COLUMNA_APELLIDOS + TIPO_TEXTO + COMA
//		    + UsuarioContract.COLUMNA_FECHA_NACIMIENTO + TIPO_TEXTO + COMA
//		    + UsuarioContract.COLUMNA_ALTURA + TIPO_REAL + COMA
//		    + UsuarioContract.COLUMNA_PESO + TIPO_REAL + COMA
//		    + UsuarioContract.COLUMNA_DONANTE + TIPO_ENTERO + COMA
//		    + UsuarioContract.COLUMNA_GRUPO_SANGUINEO + TIPO_TEXTO + " )";
////		    + UsuarioContract.COLUMNA_OTRA_INFORMACION + TIPO_TEXTO + " )";
    
    private static final String SQL_CREAR_TABLA_ALERGIAS = 
	    "CREATE TABLE " + AlergiaContract.NOMBRE_TABLA + " (" 
		    + AlergiaContract._ID + " INTEGER PRIMARY KEY," 
		    + AlergiaContract.COLUMNA_NOMBRE + TIPO_TEXTO + " )";
    
    private static final String SQL_CREAR_TABLA_ENFERMEDADES = 
	    "CREATE TABLE " + EnfermedadContract.NOMBRE_TABLA + " (" 
		    + EnfermedadContract._ID + " INTEGER PRIMARY KEY," 
		    + EnfermedadContract.COLUMNA_NOMBRE + TIPO_TEXTO + " )";
    
    private static final String SQL_CREAR_TABLA_MEDICACIONES = 
	    "CREATE TABLE " + MedicacionContract.NOMBRE_TABLA + " (" 
		    + MedicacionContract._ID + " INTEGER PRIMARY KEY," 
		    + MedicacionContract.COLUMNA_NOMBRE + TIPO_TEXTO + " )";
    
    //BORRAR TABLAS
    private static final String SQL_BORRAR_CONTACTOS = 
	    "DROP TABLE IF EXISTS " + ContactoContract.NOMBRE_TABLA;
    
    private static final String SQL_BORRAR_USUARIO = 
	    "DROP TABLE IF EXISTS " + UsuarioContract.NOMBRE_TABLA;
    
    private static final String SQL_BORRAR_ALERGIAS = 
	    "DROP TABLE IF EXISTS " + AlergiaContract.NOMBRE_TABLA;
    
    private static final String SQL_BORRAR_ENFERMEDADES = 
	    "DROP TABLE IF EXISTS " + EnfermedadContract.NOMBRE_TABLA;
    
    private static final String SQL_BORRAR_MEDICACIONES = 
	    "DROP TABLE IF EXISTS " + MedicacionContract.NOMBRE_TABLA;

    public ICEDBHelper(Context context) {
	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
	db.execSQL(SQL_CREAR_TABLA_CONTACTOS);
//	db.execSQL(SQL_CREAR_TABLA_USUARIO);
	db.execSQL(SQL_CREAR_TABLA_ALERGIAS);
	db.execSQL(SQL_CREAR_TABLA_ENFERMEDADES);
	db.execSQL(SQL_CREAR_TABLA_MEDICACIONES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	db.execSQL(SQL_BORRAR_CONTACTOS);
	db.execSQL(SQL_BORRAR_USUARIO);
	db.execSQL(SQL_BORRAR_ALERGIAS);
	db.execSQL(SQL_BORRAR_ENFERMEDADES);
	db.execSQL(SQL_BORRAR_MEDICACIONES);
        onCreate(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
