package com.miguelcg.ice.datos;

import java.util.List;

import android.content.Context;

import com.miguelcg.ice.datos.daos.AlergiaDao;
import com.miguelcg.ice.datos.daos.ContactoDao;
import com.miguelcg.ice.datos.daos.EnfermedadDao;
import com.miguelcg.ice.datos.daos.MedicacionDao;
import com.miguelcg.ice.datos.daos.OtraInformacionDao;
import com.miguelcg.ice.datos.daos.UsuarioDao;
import com.miguelcg.ice.modelo.Contacto;
import com.miguelcg.ice.modelo.propietario.Alergia;
import com.miguelcg.ice.modelo.propietario.Enfermedad;
import com.miguelcg.ice.modelo.propietario.Medicacion;
import com.miguelcg.ice.modelo.propietario.Usuario;

public class ICEManager {

    private Context contexto;

    public ICEManager(Context contexto) {
	this.contexto = contexto;
    }

    public Contacto insertarContacto(Contacto contacto) {
	return new ContactoDao(contexto).insertarContacto(contacto);
    }

    public List<Contacto> getContactos() {
	return new ContactoDao(contexto).getContactos();
    }

    public void actualizarInformacionUsuario(Usuario usuario) {
	new UsuarioDao(contexto).actualizarInformacion(usuario);
    }

    public Usuario getInformacionUsuario() {
	return new UsuarioDao(contexto).getInformacion();
    }

    public String getOtraInformacion() {
	return new OtraInformacionDao(contexto).getOtraInformacion();
    }

    public void actualizarOtraInformacion(String informacion) {
	new OtraInformacionDao(contexto).actualizarOtraInformacion(informacion);
    }

    public List<Alergia> getAlergias() {
	return new AlergiaDao(contexto).getAlergias();
    }

    public void anadirAlergia(Alergia alergia) {
	new AlergiaDao(contexto).crearAlergia(alergia);
    }

    public void anadirEnfermedad(Enfermedad enfermedad) {
	new EnfermedadDao(contexto).anadirEnfermedad(enfermedad);
    }

    public List<Enfermedad> getEnfermedades() {
	return new EnfermedadDao(contexto).getEnfermedades();
    }

    public List<Medicacion> getMedicaciones() {
	return new MedicacionDao(contexto).getMedicaciones();
    }

    public void anadirMedicacion(Medicacion medicacion) {
	new MedicacionDao(contexto).anadirMedicacion(medicacion);
    }

    public void eliminarContacto(Contacto contacto) {
	new ContactoDao(contexto).borrarContacto(contacto);
    }
    
    public void eliminarAlergia(Alergia alergia){
	new AlergiaDao(contexto).borrarAlergia(alergia);
    }
    
    public void eliminarEnfermedad(Enfermedad enfermedad){
	new EnfermedadDao(contexto).borrarEnfermedad(enfermedad);
    }
    
    public void eliminarMedicacion(Medicacion medicacion){
	new MedicacionDao(contexto).borrarMedicacion(medicacion);
    }

}
