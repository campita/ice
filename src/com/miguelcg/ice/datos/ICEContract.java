package com.miguelcg.ice.datos;

import android.provider.BaseColumns;

public final class ICEContract {

    public static abstract class ContactoContract implements BaseColumns {
	public static final String NOMBRE_TABLA = "contacto";

	public static final String COLUMNA_NOMBRE = "nombre";
	public static final String COLUMNA_URI = "uri";
    }
    
    public static abstract class UsuarioContract implements BaseColumns{
	public static final String NOMBRE_TABLA = "usuario";
	
	public static final String COLUMNA_NOMBRE = "nombre";
//	public static final String COLUMNA_APELLIDOS = "apellidos";
	public static final String COLUMNA_FECHA_NACIMIENTO = "fecha_nacimientos";
	public static final String COLUMNA_ALTURA = "altura";
	public static final String COLUMNA_PESO = "peso";
	public static final String COLUMNA_DONANTE = "donante";
	public static final String COLUMNA_GRUPO_SANGUINEO = "grupo_sanguineo";
//	public static final String COLUMNA_OTRA_INFORMACION = "otra_informacion";
    }
    
    public static abstract class OtraInformacionContract implements BaseColumns{
	public static final String NOMBRE_TABLA = "otra_informacion";
	
	public static final String COLUMNA_INFORMACION = "otra_informacion";
    }
    
    public static abstract class AlergiaContract implements BaseColumns{
	public static final String NOMBRE_TABLA = "alergia";
	
	public static final String COLUMNA_NOMBRE = "nombre";
    }
    
    public static abstract class EnfermedadContract implements BaseColumns{
	public static final String NOMBRE_TABLA = "enfermedad";
	
	public static final String COLUMNA_NOMBRE = "nombre";
    }
    
    public static abstract class MedicacionContract implements BaseColumns{
	public static final String NOMBRE_TABLA = "medicacion";
	
	public static final String COLUMNA_NOMBRE = "nombre";
    }

}
