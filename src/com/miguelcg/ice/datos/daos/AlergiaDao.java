package com.miguelcg.ice.datos.daos;

import java.util.ArrayList;
import java.util.List;

import com.miguelcg.ice.datos.ICEContract.AlergiaContract;
import com.miguelcg.ice.modelo.propietario.Alergia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class AlergiaDao extends SQLiteDao{
    
    private String[] columnas = {
	    AlergiaContract._ID,
	    AlergiaContract.COLUMNA_NOMBRE
    };

    public AlergiaDao(Context context) {
	super(context);
    }

    public Alergia crearAlergia(Alergia alergia){
	SQLiteDatabase conexion = abrir();
	
	ContentValues values = new ContentValues();
	values.put(AlergiaContract.COLUMNA_NOMBRE, alergia.getNombre());
	
	long id = conexion.insert(AlergiaContract.NOMBRE_TABLA, null, values);
	
	Cursor cursor = conexion.query(
		AlergiaContract.NOMBRE_TABLA, 
		columnas, 
		AlergiaContract._ID + " = " + id,
		null,
		null,
		null,
		null);
	
	cursor.moveToFirst();
	Alergia ret = cursorToAlergia(cursor);
	cursor.close();
	cerrar();
	
	return ret;
    }
    
    public void borrarAlergia(Alergia alergia){
	SQLiteDatabase conexion = abrir();
	long id = alergia.getId();
	
	conexion.delete(
		AlergiaContract.NOMBRE_TABLA, 
		AlergiaContract._ID + " = " + id, 
		null);
	
	cerrar();
    }
    
    public List<Alergia> getAlergias() {
	SQLiteDatabase conexion = abrir();
	
	List<Alergia> alergias = new ArrayList<Alergia>();

	Cursor cursor = conexion.query(
		AlergiaContract.NOMBRE_TABLA,
		columnas, 
		null, 
		null, 
		null, 
		null, 
		null);
	
	if(cursor.moveToFirst()){
	    do{
		Alergia alergia = cursorToAlergia(cursor);
		alergias.add(alergia);
    
	    }while (cursor.moveToNext());
	}
	
	cursor.close();
	cerrar();
	
	return alergias;
}
    
    private Alergia cursorToAlergia(Cursor cursor) {
	Alergia alergia = new Alergia();
	
	alergia.setId(cursor.getLong(0));
	alergia.setNombre(cursor.getString(1));
	
	return alergia;
    }
}
