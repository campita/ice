package com.miguelcg.ice.datos.daos;

import com.miguelcg.ice.R;
import com.miguelcg.ice.datos.ICEContract.OtraInformacionContract;

import android.content.Context;
import android.content.SharedPreferences.Editor;

public class OtraInformacionDao extends SharedPreferencesDao {

    public OtraInformacionDao(Context contexto) {
	super(contexto, R.string.nombre_fichero_datos_otra_informacion);
    }

    public void actualizarOtraInformacion(String informacion) {
	Editor editor = abrir().edit();
	
	editor.putString(OtraInformacionContract.COLUMNA_INFORMACION, informacion);
	editor.commit();
    }

    public String getOtraInformacion() {
	return abrir().getString(OtraInformacionContract.COLUMNA_INFORMACION, "");
    }

}
