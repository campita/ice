package com.miguelcg.ice.datos.daos;

import java.util.ArrayList;
import java.util.List;

import com.miguelcg.ice.datos.ICEContract.MedicacionContract;
import com.miguelcg.ice.modelo.propietario.Medicacion;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MedicacionDao extends SQLiteDao {
    
    private String[] columnas = {
	    MedicacionContract._ID,
	    MedicacionContract.COLUMNA_NOMBRE
    };

    public MedicacionDao(Context context) {
	super(context);
    }

    public List<Medicacion> getMedicaciones() {
	SQLiteDatabase conexion = abrir();
	
	List<Medicacion> medicaciones = new ArrayList<Medicacion>();

	Cursor cursor = conexion.query(
		MedicacionContract.NOMBRE_TABLA,
		columnas, 
		null, 
		null, 
		null, 
		null, 
		null);
	
	if(cursor.moveToFirst()){
	    do{
		Medicacion medicacion = cursorToMedicacion(cursor);
		medicaciones.add(medicacion);
    
	    }while (cursor.moveToNext());
	}
	
	cursor.close();
	cerrar();
	
	return medicaciones;
    }

    public Medicacion anadirMedicacion(Medicacion medicacion) {
	SQLiteDatabase conexion = abrir();
	
	ContentValues values = new ContentValues();
	values.put(MedicacionContract.COLUMNA_NOMBRE, medicacion.getNombre());
	
	long id = conexion.insert(MedicacionContract.NOMBRE_TABLA, null, values);
	
	Cursor cursor = conexion.query(
		MedicacionContract.NOMBRE_TABLA, 
		columnas, 
		MedicacionContract._ID + " = " + id,
		null,
		null,
		null,
		null);
	
	cursor.moveToFirst();
	Medicacion ret = cursorToMedicacion(cursor);
	cursor.close();
	cerrar();
	
	return ret;
    }

    private Medicacion cursorToMedicacion(Cursor cursor) {
	Medicacion medicacion = new Medicacion();
	
	medicacion.setId(cursor.getLong(0));
	medicacion.setNombre(cursor.getString(1));
	
	return medicacion;
    }
    
    public void borrarMedicacion(Medicacion medicacion){
   	SQLiteDatabase conexion = abrir();
   	
   	long id = medicacion.getId();
   	
   	conexion.delete(
   		MedicacionContract.NOMBRE_TABLA, 
   		MedicacionContract._ID + " = " + id, 
   		null);
   	
   	cerrar();
       }

}
