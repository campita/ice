package com.miguelcg.ice.datos.daos;

import com.miguelcg.ice.R;
import com.miguelcg.ice.datos.ICEContract.UsuarioContract;
import com.miguelcg.ice.modelo.propietario.GrupoSanguineo;
import com.miguelcg.ice.modelo.propietario.Usuario;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class UsuarioDao extends SharedPreferencesDao{

    public UsuarioDao(Context context) {
	super(context, R.string.nombre_fichero_datos_usuario);
    }
    
    public void actualizarInformacion(Usuario usuario){
	Editor editor = abrir().edit();
	
	editor.putString(UsuarioContract.COLUMNA_NOMBRE, usuario.getNombre());
	editor.putString(UsuarioContract.COLUMNA_FECHA_NACIMIENTO, usuario.getFechaNacimiento());
	editor.putFloat(UsuarioContract.COLUMNA_ALTURA, usuario.getAltura());
	editor.putFloat(UsuarioContract.COLUMNA_PESO, usuario.getPeso());
	editor.putBoolean(UsuarioContract.COLUMNA_DONANTE, usuario.isDonante());
	editor.putInt(UsuarioContract.COLUMNA_GRUPO_SANGUINEO, usuario.getGrupoSanguineo().ordinal());
	
	editor.commit();
    }
    
    public Usuario getInformacion(){
	Usuario usuario = new Usuario();
	
	SharedPreferences lector = abrir();
	
	usuario.setNombre(lector.getString(UsuarioContract.COLUMNA_NOMBRE, ""));
	usuario.setFechaNacimiento(lector.getString(UsuarioContract.COLUMNA_FECHA_NACIMIENTO, "14/02/1991"));
	usuario.setAltura(lector.getFloat(UsuarioContract.COLUMNA_ALTURA, 0));
	usuario.setPeso(lector.getFloat(UsuarioContract.COLUMNA_PESO, 0));
	usuario.setDonante(lector.getBoolean(UsuarioContract.COLUMNA_DONANTE, true));
	usuario.setGrupoSanguineo(GrupoSanguineo.values()[lector.getInt(UsuarioContract.COLUMNA_GRUPO_SANGUINEO, 0)]);
	
	return usuario;
    }

}
