package com.miguelcg.ice.datos.daos;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.miguelcg.ice.datos.ICEDBHelper;

public class SQLiteDao {

    private SQLiteDatabase db;
    private ICEDBHelper dbHelper;
    
    protected SQLiteDao(Context context){
	dbHelper = new ICEDBHelper(context);
    }

    protected SQLiteDatabase abrir() throws SQLException {
	if(db == null || !db.isOpen()){
	    db = dbHelper.getWritableDatabase();
	}
	return db;
    }

    protected void cerrar() {
	db.close();
	dbHelper.close();
    }

}
