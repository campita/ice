package com.miguelcg.ice.datos.daos;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesDao {
    
    private Context contexto;
    
    private SharedPreferences sharedPreferences;
    private int idNombreFichero;
    
    public SharedPreferencesDao(Context contexto, int idNombreFichero) {
	this.contexto = contexto;
	this.idNombreFichero = idNombreFichero;
    }
    
    protected SharedPreferences abrir(){
	sharedPreferences = contexto.getSharedPreferences(
		contexto.getString(idNombreFichero), 
		Context.MODE_PRIVATE);

	return sharedPreferences;
    }

}
