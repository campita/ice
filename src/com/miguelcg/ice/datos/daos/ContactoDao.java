package com.miguelcg.ice.datos.daos;

import java.util.ArrayList;
import java.util.List;

import com.miguelcg.ice.datos.ICEContract.ContactoContract;
import com.miguelcg.ice.modelo.Contacto;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class ContactoDao extends SQLiteDao{

    private String[] columnas = { 
	    ContactoContract._ID,
	    ContactoContract.COLUMNA_NOMBRE,
	    ContactoContract.COLUMNA_URI
    };

    public ContactoDao(Context context) {
	super(context);
    }
    
    public Contacto insertarContacto(Contacto contacto){
	SQLiteDatabase conexion = abrir();
	
	ContentValues values = new ContentValues();
	values.put(ContactoContract.COLUMNA_NOMBRE, contacto.getNombre());
	values.put(ContactoContract.COLUMNA_URI, contacto.getUri().toString());
	
	long id = conexion.insert(ContactoContract.NOMBRE_TABLA, null, values);
	
	Cursor cursor = conexion.query(
		ContactoContract.NOMBRE_TABLA, 
		columnas, 
		ContactoContract._ID + " = " + id,
		null,
		null,
		null,
		null);
	
	cursor.moveToFirst();
	Contacto contactoRet = cursorToContacto(cursor);
	cursor.close();
	cerrar();
	
	return contactoRet;
    }
    
    public void borrarContacto(Contacto contacto){
	SQLiteDatabase conexion = abrir();
	
	long id = contacto.getId();
	
	conexion.delete(
		ContactoContract.NOMBRE_TABLA, 
		ContactoContract._ID + " = " + id, 
		null);
	
	cerrar();
    }
    
    public List<Contacto> getContactos() {
	SQLiteDatabase conexion = abrir();
	
	    List<Contacto> contactos = new ArrayList<Contacto>();

	    Cursor cursor = conexion.query(
		    ContactoContract.NOMBRE_TABLA,
		    columnas, 
		    null, 
		    null, 
		    null, 
		    null, 
		    null);

	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
		Contacto contacto = cursorToContacto(cursor);
		contactos.add(contacto);
		cursor.moveToNext();
	    }

	    cursor.close();
	    cerrar();
	    return contactos;
    }
    
    private Contacto cursorToContacto(Cursor cursor) {
	Contacto contacto = new Contacto();
	contacto.setId(cursor.getLong(0));
	contacto.setNombre(cursor.getString(1));
	contacto.setUri(Uri.parse(cursor.getString(2)));
	
	return contacto;
    }
}
