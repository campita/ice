package com.miguelcg.ice.datos.daos;

import java.util.ArrayList;
import java.util.List;

import com.miguelcg.ice.datos.ICEContract.EnfermedadContract;
import com.miguelcg.ice.modelo.propietario.Enfermedad;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class EnfermedadDao extends SQLiteDao {
    
    private String[] columnas = {
	    EnfermedadContract._ID,
	    EnfermedadContract.COLUMNA_NOMBRE
    };

    public EnfermedadDao(Context context) {
	super(context);
    }

    public Enfermedad anadirEnfermedad(Enfermedad enfermedad) {
	SQLiteDatabase conexion = abrir();
	
	ContentValues values = new ContentValues();
	values.put(EnfermedadContract.COLUMNA_NOMBRE, enfermedad.getNombre());
	
	long id = conexion.insert(EnfermedadContract.NOMBRE_TABLA, null, values);
	
	Cursor cursor = conexion.query(
		EnfermedadContract.NOMBRE_TABLA, 
		columnas, 
		EnfermedadContract._ID + " = " + id,
		null,
		null,
		null,
		null);
	
	cursor.moveToFirst();
	Enfermedad ret = cursorToEnfermedad(cursor);
	cursor.close();
	cerrar();
	
	return ret;
    }
    
    private Enfermedad cursorToEnfermedad(Cursor cursor) {
	Enfermedad enfermedad = new Enfermedad();
	enfermedad.setId(cursor.getLong(0));
	enfermedad.setNombre(cursor.getString(1));
	
	return enfermedad;
    }

    public List<Enfermedad> getEnfermedades() {
	SQLiteDatabase conexion = abrir();
	
	List<Enfermedad> enfermedades = new ArrayList<Enfermedad>();

	Cursor cursor = conexion.query(
		EnfermedadContract.NOMBRE_TABLA,
		columnas, 
		null, 
		null, 
		null, 
		null, 
		null);
	
	if(cursor.moveToFirst()){
	    do{
		Enfermedad enfermedad = cursorToEnfermedad(cursor);
		enfermedades.add(enfermedad);
    
	    }while (cursor.moveToNext());
	}
	
	cursor.close();
	cerrar();
	
	return enfermedades;
    }
    
    public void borrarEnfermedad(Enfermedad enfermedad){
   	SQLiteDatabase conexion = abrir();
   	
   	long id = enfermedad.getId();
   	
   	conexion.delete(
   		EnfermedadContract.NOMBRE_TABLA, 
   		EnfermedadContract._ID + " = " + id, 
   		null);
   	
   	cerrar();
   	}

}
